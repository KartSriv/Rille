import Tkinter
import tkMessageBox
# Say and say
def Say(input):
    print input

def say(input):
    print input

# Reverse
def Reverse(word):
    print word[::-1]

def reverse(word):
    print word[::-1]

#Whatis

def Whatis(man):
   if man=="Say" or man=="say":
       print "Say is a function in python used for printing what the user wants."
   elif man=="Reverse" or man=="reverse":
       print "Reverse is a function in python used for reversing the string or any other inputs."
   else :
       print "Error 2: Invalid opration(s) given to Whatis"

def whatis(man):
   if man=="Say" or man=="say":
       print "Say is a function in python used for printing what the user wants."
   elif man=="Reverse" or man=="reverse":
       print "Reverse is a function in python used for reversing the string or any other inputs."
   else :
       "Error 2: Invalid opration(s) given to whatis"

#Make
def Make(type):
    if type=="Window" or type=="windows":
        top = Tkinter.Tk()
        top.mainloop()
    elif type=="WindowsWithButton" or type=="windowwithbutton":
        top = Tkinter.Tk()

        def helloCallBack():
            tkMessageBox.showinfo("Hello Python", "Hello World")

        B = Tkinter.Button(top, text="Hello", command=helloCallBack)

        B.pack()
        top.mainloop()
    else:
        print "Error 1: Invalid Opration(s) given to make."
