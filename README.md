# Rille
A set of simple python codes which makes programming very effective.


## What exactly this Repo is all about?

This is just a set of python function which does the same job done but just a little easier.

### Why do we even need to use it?

Come on! Let's see what this is all about. When we see commands such as "Say" they look lame. Example:-

```
>>> Say ("Hello World")
Hello World
```

Commands like the above are dumb. We can just do the same by typing:-

```
>>> print "Hello World"
Hello World
```

but the actual use starts here:-

```
>>> Reverse ("Hello World")
dlrow olleH
```

or 

```
>>> word="Hello World"
>>> print word[::-1]
dlroW olleH
```
now which is easier. Rille is for small children or even any others who want to program in a easier way. Rille is very easy to understand.

### Installing

Download the Repo and extract it in the project folder.

## Using Rille

add this command at the top:-

```
from Rille import *
```

and then you can use all commands

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### Meaning of Rille

The meaning of Rille is "a fissure or narrow channel on the moon's surface."

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Python](https://www.python.org/) - The platform

## Contributing

Thanks to [Billie Thompson](https://github.com/PurpleBooth) for the readme.md template.

## Authors

* **Karthik Srivijay** - *Initial work* - [karthiksrivijay](https://github.com/karthiksrivijay)
* **Bilohit Ghosh** - *Initial work* - [Bilohit98](https://github.com/Bilohit98)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
